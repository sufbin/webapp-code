set baseDIR=D:\A\Work\office\acecicd\attempt4
set iisDIR=%baseDIR%\iis
set iis-archiveDIR=%baseDIR%\iis-archive
set scmDIR=%baseDIR%\jenkins-slave\workspace\test

echo cd to main directory
cd %baseDIR%

echo backup existing files from iis to iis-archive folder
Xcopy %iisDIR%\ %iis-archiveDIR%\ /E /H /C /I /Q /Y

echo delete existing files and folders from iis folder
del /q %iisDIR%
RMDIR /Q/S %iisDIR%\bin

echo copy files from scm to iis folder
XCopy %scmDIR%\ %iisDIR%\ /E /H /C /I /Q /Y

echo delete unwanted files from iis folder
del /q /f %iisDIR%\.gitignore %iisDIR%\updatewebsite.bat

echo delete unwanted folders from iis folder
RMDIR /Q/S %iisDIR%\.git